import copy
from decimal import Decimal
from functools import reduce

# необходимая степень округления 
o = 4

# номер варианта
number = 0

# матрица для решения
matrix :list[list[float]]= [
    [1.2431,0.1052,-0.1248,-0.1087,1.2564],
    [-0.1184,-1.1056,0.1065,0.1163,-0.9636],
    [0.1098,-0.1136,-1.0973,0.1266,0.9437],
    [-0.1253,0.1024,-0.1189,1.1854,-1.0634]
]

# переменные для преобразования матрицы согласно варианту
var_number = number * 0.01
var_dict:dict[int,tuple]= {
    0:(0,3,4),
    1:(1,2,4),
    2:(0,2,4),
    3:(1,3,4)
}

# преобразуем числа согласно варианту
for i in var_dict.keys():
    for v in var_dict[i]:
        matrix[i][v] += var_number

# проверяем на квадратность и наличие нулей
def check_zero(matrix:list[list[float]]):
    if o not in range(0,26):
        raise Exception("'o' variable need to be a natural number in range from 0 to 25 in")
    test_matrix = copy.deepcopy(matrix)
    test_matrix = list(map(lambda x: x[0:-1],test_matrix))
    m_len = len(test_matrix)
    row_c = 0
    for row in test_matrix:
        if len(row) != m_len or row[row_c] == 0:
            return False
        row_c +=1
    return True


# функция которая возращает ряд с нулевым коэффицентом(номер коэффицента) номер коэффицента задаётся в значении step_number
# _base_row это ряд
def gauss_step(
    _base_row:list[Decimal], # уравнение где необходимый коэффицент уже равняется 1
    _change_row:list[Decimal], # уравнение где необходимый коэффицент нужно приравнять к 0
    step_number:int # номер шага
    ) -> list[Decimal]:
    base_row = _base_row.copy()
    change_row = _change_row.copy()
    count = 0

    # коэффицент x(номер шага) из ряда у которого этот разряд будет приравнен к нулю
    x1 = change_row[step_number]
    if x1 == 0:
        return change_row

    # проходимся по каждому коэффиценту уравнения (номер шага+1) и домножаем на x1 для последующего вычитания
    for num in base_row:
        base_row[count] = round(x1*num,o)
        count += 1


    count = 0

    # вычитаем из уравнения номер (номер шага+1) уравнение номер (номер шага+2) для исчезновения коэффицента x(номер шага)
    for num in change_row:
        change_row[count] = round(Decimal(change_row[count] - base_row[count]),o)
        count += 1
    
    return change_row


# функция которая преобразует начальную матрицу в треугольную
def gauss(matrix:list[list[float]]):
    if not check_zero(matrix):
        raise Exception("Wrong matrix, it not square or exist zero fields")
    mt = copy.deepcopy(matrix)
    mt = list(map(lambda l:list(map(lambda x:Decimal(round(Decimal(x),o)),l)),mt))

    # номер уравнения
    row_count = 0

    # проходимся по каждому уравнению системы
    for row in mt:
        num_count = 0

        # номер равняется коэффиценту разряда уравнения под номером (номер уравнения) x(номер уравнения)
        number = mt[row_count][row_count]

        # проходимся по каждому коэффиценту уравнения
        for num in row:
            # делим каждый коэффицент уравнения на значение коэффицента (number)
            mt[row_count][num_count] = round(mt[row_count][num_count]/number,o)
            num_count+=1
        row_count+=1

        # после того как мы разделили все коэффиценты уравнения на коэффицент (number)
        # переходим к шагу номер 2 где каждый коэффицент (number) во всех следующих уравнениях будет приравнен к нулю
        for erow in range(row_count,len(mt)):
            mt[erow] = gauss_step(mt[row_count-1],mt[erow],step_number=row_count-1)
            

    return mt

    
    


# функция для получения из треугольной матрицы искомых значений (x1,x2,x3, ...)
def get_result(matrix:list[list[Decimal]]):

    # получаем значени матрицы в перевёрнутом виде, где последнее уравнение будет первым в списке, второе предпоследним и так далее
    mt = matrix.copy()[::-1]

    # отсекаем все нулевые значения которые у нас присутсвуют в уравнении
    mt = list(map(lambda l:list(filter(lambda x: x != Decimal(0),l)),mt))

    # наши искомые значения
    results = []

    # номер уравнения
    row_count = 0
    for row in mt:

        # получаем все значения уравнения кроме первого и последнего 
        values = row[1:-1]

        # если это последнее уравнение где нет необходимости производить арифметические операции
        # то просто добавляем первое значение в наши results
        if len(values) == 0:
            results.append(row[-1])
            continue


        value_count = 0
        x = 0

        # если же всё таки нам необходимо выполнить некоторые действия для получения искомого значения
        # то из отсечённые значения которые мы получили выше мы домножаем на уже известные нам значение из results
        for value in values:
            # переворачиваем значения results так как добавляем значения мы начиная в последнего
            # а в уравнении они идут по порядку
            prevx = results[::-1]

            # прибавляем к нашей разницы значение соответсвующего коэффицента помноженное 
            # на соответствующее значение икса которое нам уже известно
            x += round(value * prevx[value_count],o)
            value_count += 1
        
        # добавляем в список значений разницу между суммой наших произведений и значением уравнения 
        results.append(row[-1] - x)

    # return results[::-1]
    return list(map(lambda x: float(x),results[::-1]))


#Показываем список значений
result = get_result(gauss(matrix))
print(get_result(gauss(matrix)))


def summ(prev,cur):
    return prev + cur

def check(matrix:list[list[float]],values:list[float]):
    row_count = 0
    gauss_matrix = list(map(lambda x : list(map(lambda n : round(n,o),x)),copy.deepcopy(matrix)))
    l = len(matrix)
    for row in gauss_matrix:
        num_count = 0
        for num in range(0,l):
            gauss_matrix[row_count][num_count] *= values[num_count]
            num_count+=1
        s = round(reduce(summ,gauss_matrix[row_count][:l]),o)
        gauss_matrix[row_count] = [s,gauss_matrix[row_count][-1]]
        print(gauss_matrix[row_count])
        row_count += 1
    

check(matrix,result)